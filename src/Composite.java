import java.util.ArrayList;
import java.util.List;

public class Composite implements Component{
    public Composite(String name) {
        this.name = name;
    }
    private  int prixtotal=0;
    private  String name ;
    List<Component> componentList = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addComponent(Component component){
        componentList.add(component);
    }
    public void  removeComponent(Component component){
        componentList.remove(component);
    }
    @Override
    public int prix() {

        for (Component component:componentList) {
            prixtotal+=component.prix();
        }
        System.out.println("le prix total de  "+ name+" "+prixtotal);
        System.out.println("-----------------------------------------");
        return  prixtotal;
    }
}
