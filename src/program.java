public class program {
    public static void main(String[] args) {
        Component cabledata= new leaf("cabledata", 100);
        Component chargeuradaptateur= new leaf("adaptateursecteur",50);
        Composite chargeur = new Composite("chargeur");
        Component ecouteurs= new leaf("ecouteur samsung", 142);
        Component telephone = new leaf("samsung s6",1520);
        Composite livrable = new Composite("livraison telephone");
        //
        chargeur.addComponent(cabledata);
        chargeur.addComponent(chargeuradaptateur);
        livrable.addComponent(telephone);
        livrable.addComponent(chargeur);
        livrable.addComponent(ecouteurs);

        livrable.prix();
    }
}
